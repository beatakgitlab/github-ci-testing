'use strict';

const http = require('http');
const Koa = require('koa');
const app = new Koa();

const Router = require('koa-joi-router');
const Joi = Router.Joi;

const HTTP_PORT = (() => { const p = parseInt(process.env['PORT'], 10); return isNaN(p) ? 3000 : p;})();

const server = http.createServer(app.callback());

// koa seems all about middleware
// https://github.com/koajs/koa/wiki

const router = Router();

app.use(router.middleware());

router.get('/', async (ctx) => {
  // console.log(`Request to "${ctx.request.method} ${ctx.request.url}"`);
  ctx.body = 'hello joi-router!';
});

router.route({
  method: 'post',
  path: '/signup',
  validate: {
    body: {
      name: Joi.string().max(100),
      email: Joi.string().lowercase().email(),
      // password: Joi.string().max(100),
      // _csrf: Joi.string().token()
    },
    type: 'json',
    output: {
      200: {
        body: {
          userId: Joi.string(),
          name: Joi.string()
        }
      }
    }
  },
  handler: async (ctx) => {
    const user = await createUser(ctx.request.body);
    ctx.status = 201;
    ctx.body = user;
  }
});

const createUser = async function (data) {
  console.log('createUser', data);
  return new Promise((res, _rej) => {
    return res(JSON.stringify( {id: 1, name: 'user fucker'} ));
  });
}

server.listen(
  HTTP_PORT,
  function () {
    console.log(`start to listen to ${HTTP_PORT}`);
  }
);
